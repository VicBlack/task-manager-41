# TASK MANAGER

## DEVELOPER INFO

**NAME**: Anton Kupriyanov

**EMAIL**: ankupr29@gmail.com

**EMAIL**: akupriyanov@t1-consulting.ru

## SOFTWARE

**JAVA**: 1.8 OPENJDK

**OS**: Windows 10

## HARDWARE

**CPU**: i7

**RAM**: 16GB

**SSD**: 256GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./task-manager.jar
```
