package ru.t1.kupriyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.kupriyanov.tm.command.AbstractCommand;
import ru.t1.kupriyanov.tm.dto.model.ProjectDTO;
import ru.t1.kupriyanov.tm.enumerated.Role;
import ru.t1.kupriyanov.tm.enumerated.Status;

public abstract class AbstractProjectCommand extends AbstractCommand {

//    @NotNull
//    protected IProjectService getProjectService() {
//        return getServiceLocator().getProjectService();
//    }
//
//    @NotNull
//    protected IProjectTaskService getProjectTaskService() {
//        return getServiceLocator().getProjectTaskService();
//    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return getServiceLocator().getProjectEndpoint();
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("ID: + " + project.getId());
        System.out.println("NAME: + " + project.getName());
        System.out.println("DESCRIPTION: + " + project.getDescription());
        System.out.println("STATUS: + " + Status.toName(project.getStatus()));
    }

}
