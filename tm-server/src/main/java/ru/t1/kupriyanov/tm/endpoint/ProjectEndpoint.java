package ru.t1.kupriyanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.kupriyanov.tm.api.service.IProjectService;
import ru.t1.kupriyanov.tm.api.service.IServiceLocator;
import ru.t1.kupriyanov.tm.dto.request.*;
import ru.t1.kupriyanov.tm.dto.response.*;
import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.dto.model.ProjectDTO;
import ru.t1.kupriyanov.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.kupriyanov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDTO project = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDTO project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        getProjectService().removeAll(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectGetByIdResponse getProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectGetByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project = getProjectService().findOneById(userId, id);
        return new ProjectGetByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectGetByIndexResponse getProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectGetByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final ProjectDTO project = getProjectService().findOneByIndex(userId, index);
        return new ProjectGetByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final List<ProjectDTO> projects = getProjectService().findAll(userId);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project = getProjectService().removeOneById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final ProjectDTO project = getProjectService().removeOneByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

}
