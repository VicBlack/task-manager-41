package ru.t1.kupriyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    ProjectDTO add(@Nullable ProjectDTO project);

    @Nullable
    ProjectDTO add(
            @Nullable String userId,
            @Nullable ProjectDTO project
    );

    @NotNull
    Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId);

    @Nullable
    ProjectDTO findOneById(@Nullable String id);

    @Nullable
    ProjectDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    ProjectDTO findOneByIndex(@Nullable Integer index);

    @Nullable
    ProjectDTO findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    ProjectDTO removeOne(@Nullable ProjectDTO project);

    ProjectDTO removeOne(
            @Nullable String userId,
            @Nullable ProjectDTO project
    );

    @Nullable
    ProjectDTO removeOneById(@Nullable String id);

    @Nullable
    ProjectDTO removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    ProjectDTO removeOneByIndex(@Nullable Integer index);

    @Nullable
    ProjectDTO removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    void removeAll();

    void removeAll(@Nullable String userId);

    int getSize();

    int getSize(@Nullable String userId);

    boolean existsById(String id);

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );


}
