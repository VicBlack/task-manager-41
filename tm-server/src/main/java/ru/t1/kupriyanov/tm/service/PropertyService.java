package ru.t1.kupriyanov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kupriyanov.tm.api.service.IPropertyService;

import java.util.Properties;
import java.util.jar.Manifest;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "author.email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "author.name";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "34534";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "1111";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    public static final String SESSION_KEY = "session.key";

    @NotNull
    public static final String SESSION_KEY_DEFAULT = "asdwqezxc123";

    @NotNull
    public static final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    public static final String SESSION_TIMEOUT_DEFAULT = "10000";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String DB_USER = "database.username";

    @NotNull
    public static final String DB_USER_DEFAULT = "postgres";

    @NotNull
    public static final String DB_PASSWORD = "database.password";

    @NotNull
    public static final String DB_PASSWORD_DEFAULT = "admin";

    @NotNull
    public static final String DB_URL = "database.url";

    @NotNull
    public static final String DB_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    public static final String DB_DRIVER_KEY = "database.driver";

    @NotNull
    public static final String DB_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    public static final String DB_DIALECT_KEY = "database.dialect";

    @NotNull
    public static final String DB_HBM2DDL_AUTO_DEFAULT = "update";

    @NotNull
    public static final String DB_HBM2DDL_AUTO_KEY = "database.hbm2ddl_auto";

    @NotNull
    public static final String DB_SHOW_SQL_DEFAULT = "false";

    @NotNull
    public static final String DB_SHOW_SQL_KEY = "database.show_sql";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, SESSION_TIMEOUT_DEFAULT);
    }


    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUser() {
        return getStringValue(DB_USER);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DB_PASSWORD);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DB_URL);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DB_DRIVER_KEY, DB_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDialect() {
        return getStringValue(DB_DIALECT_KEY, DB_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseHBM2DDLAuto() {
        return getStringValue(DB_HBM2DDL_AUTO_KEY, DB_HBM2DDL_AUTO_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseShowSQL() {
        return getStringValue(DB_SHOW_SQL_KEY, DB_SHOW_SQL_DEFAULT);
    }

}
