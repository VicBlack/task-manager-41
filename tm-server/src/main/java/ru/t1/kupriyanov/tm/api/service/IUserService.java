package ru.t1.kupriyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.enumerated.Role;
import ru.t1.kupriyanov.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserService extends IService<UserDTO> {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    UserDTO add(@Nullable UserDTO user);

    @NotNull
    Collection<UserDTO> add(@Nullable Collection<UserDTO> users);

    @NotNull
    Collection<UserDTO> set(@NotNull Collection<UserDTO> users);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO removeOne(@Nullable UserDTO model);

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    @Nullable
    UserDTO removeByEmail(@Nullable String email);

    void removeAll();

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @NotNull
    UserDTO lockUserByLogin(@Nullable String login);

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String login);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

}
