package ru.t1.kupriyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.model.SessionDTO;
import ru.t1.kupriyanov.tm.dto.model.UserDTO;
import ru.t1.kupriyanov.tm.model.Session;
import ru.t1.kupriyanov.tm.model.User;

public interface IAuthService {

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    void logout(@Nullable SessionDTO session);

}
