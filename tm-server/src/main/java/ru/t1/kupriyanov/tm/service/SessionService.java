package ru.t1.kupriyanov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.repository.ISessionRepository;
import ru.t1.kupriyanov.tm.api.service.IConnectionService;
import ru.t1.kupriyanov.tm.api.service.ISessionService;
import ru.t1.kupriyanov.tm.exception.entity.ModelNotFoundException;
import ru.t1.kupriyanov.tm.exception.field.IdEmptyException;
import ru.t1.kupriyanov.tm.exception.field.IndexIncorrectException;
import ru.t1.kupriyanov.tm.exception.field.UserIdEmptyException;
import ru.t1.kupriyanov.tm.dto.model.SessionDTO;

import java.util.Collection;
import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    protected final IConnectionService connectionService;

    public SessionService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public SessionDTO add(@Nullable final SessionDTO session) {
        if (session == null) throw new ModelNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.add(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return session;
    }

    @Nullable
    @Override
    public SessionDTO add(
            @Nullable final String userId,
            @Nullable final SessionDTO session
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new ModelNotFoundException();

        session.setUserId(userId);

        return add(session);
    }

    @NotNull
    @Override
    public Collection<SessionDTO> add(@Nullable final Collection<SessionDTO> sessions) {
        if (sessions == null) throw new ModelNotFoundException();

        for (final SessionDTO session : sessions) {
            add(session);
        }

        return sessions;
    }

    @NotNull
    @Override
    public Collection<SessionDTO> set(@Nullable final Collection<SessionDTO> sessions) {
        if (sessions == null) throw new ModelNotFoundException();

        removeAll();

        return add(sessions);
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);

        try {
            return sessionRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return sessionRepository.findAllByUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return sessionRepository.findOneById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return sessionRepository.findOneByIdByUserId(userId, id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<SessionDTO> sessions = findAll();
        return sessions.get(index);
    }

    @Nullable
    @Override
    public SessionDTO findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();

        @NotNull final List<SessionDTO> sessions = findAll(userId);
        return sessions.get(index);
    }

    @Override
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.removeAllByUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO removeOne(@Nullable final SessionDTO session) {
        if (session == null) throw new ModelNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.removeOneById(session.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return session;
    }

    @Override
    public SessionDTO removeOne(
            @Nullable final String userId,
            @Nullable final SessionDTO session
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new ModelNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.removeOneByIdByUserId(userId, session.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return session;
    }

    @Nullable
    @Override
    public SessionDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable SessionDTO session = findOneById(id);
        if (session == null) return null;

        return removeOne(session);
    }

    @Nullable
    @Override
    public SessionDTO removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable SessionDTO session = findOneById(userId, id);
        if (session == null) return null;

        return removeOne(userId, session);
    }

    @Nullable
    @Override
    public SessionDTO removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<SessionDTO> sessions = findAll();
        return removeOneById(sessions.get(index).getId());
    }

    @Nullable
    @Override
    public SessionDTO removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<SessionDTO> sessions = findAll(userId);
        return removeOneById(sessions.get(index).getId());
    }

    @Override
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return sessionRepository.getSize();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return sessionRepository.getSizeByUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return findOneById(id) != null;
    }

}
