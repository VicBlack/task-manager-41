package ru.t1.kupriyanov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.model.IWBS;
import ru.t1.kupriyanov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "TASK")
public final class TaskDTO extends AbstractUserOwnedModelDTO implements IWBS {

    @NotNull
    @Column(name = "NAME")
    private String name = "";

    @NotNull
    @Column(name = "DESCRIPTION")
    private String description = "";

    @NotNull
    @Column(name = "STATUS")
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "PROJECT_ID")
    private String projectId;

    @NotNull
    @Column(name = "CREATED")
    private Date created = new Date();

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}