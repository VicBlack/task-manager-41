package ru.t1.kupriyanov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.enumerated.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "USER")
public final class UserDTO extends AbstractModelDTO {

    @Nullable
    @Column(name = "LOGIN")
    private String login;

    @Nullable
    @Column(name = "PASSWORD_HASH")
    private String passwordHash;

    @Nullable
    @Column(name = "EMAIL")
    private String email;

    @Nullable
    @Column(name = "FIRST_NAME")
    private String firstName;

    @Nullable
    @Column(name = "LAST_NAME")
    private String lastName;

    @Nullable
    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @NotNull
    @Column(name = "ROLE")
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = "LOCKED")
    private Boolean locked = false;

}
