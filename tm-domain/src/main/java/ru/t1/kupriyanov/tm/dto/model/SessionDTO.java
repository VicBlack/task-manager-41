package ru.t1.kupriyanov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.enumerated.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "SESSION")
public final class SessionDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    @Column(name = "DATE")
    private Date date = new Date();

    @Nullable
    @Column(name = "ROLE")
    private Role role = null;

}
