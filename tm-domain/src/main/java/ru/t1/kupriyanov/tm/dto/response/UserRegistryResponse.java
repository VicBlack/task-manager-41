package ru.t1.kupriyanov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.kupriyanov.tm.dto.model.UserDTO;
import ru.t1.kupriyanov.tm.model.User;

@NoArgsConstructor
public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@NotNull UserDTO user) {
        super(user);
    }

}
