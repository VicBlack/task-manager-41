package ru.t1.kupriyanov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.model.TaskDTO;
import ru.t1.kupriyanov.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIdResponse extends AbstractTaskResponse {

    public TaskRemoveByIdResponse(@Nullable TaskDTO task) {
        super(task);
    }

}
