package ru.t1.kupriyanov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.kupriyanov.tm.api.model.IWBS;
import ru.t1.kupriyanov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "PROJECT")
public final class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    @NotNull
    @Column(name = "NAME")
    private String name = "";

    @NotNull
    @Column(name = "DESCRIPTION")
    private String description = "";

    @NotNull
    @Column(name = "STATUS")
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "CREATED")
    private Date created = new Date();

    public ProjectDTO(
            @NotNull String name,
            @NotNull Status status
    ) {
        this.name = name;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}